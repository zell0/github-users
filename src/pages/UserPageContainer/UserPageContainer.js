import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import Loader from 'components/Loader';
import NotFoundPage from 'pages/NotFoundPage';
import ErrorIndicator from 'components/ErrorIndicator';
import User from 'components/User';
import { userFetchStart, useUser } from 'ducks/user';

const UserPageContainer = () => {
  const { user: username } = useParams();
  const dispatch = useDispatch();
  const { loading, error, user } = useUser();

  useEffect(() => {
    dispatch(userFetchStart(username));
  }, [dispatch, username]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    if (error.message === '404') {
      return <NotFoundPage />;
    }

    return <ErrorIndicator message={error.message} />;
  }

  return (
    user && (
      <User user={user} />
    )
  );
};

export default UserPageContainer;
