import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';

import Loader from 'components/Loader';
import ErrorIndicator from 'components/ErrorIndicator';
import NotFoundPage from 'pages/NotFoundPage';
import UserList from 'components/UserList';
import { userListFetchStart, useUserList } from 'ducks/userList';

const HomePageContainer = () => {
  const {
    loading, error, userList, page, perPage, total
  } = useUserList();
  const dispatch = useDispatch();
  const history = useHistory();
  const params = useParams();
  const [pageParam, setPageParam] = useState(Number(params.page) || 1);
  const pages = Math.floor(total / perPage);

  const handleChangePage = ({ selected }) => {
    const pageNum = selected + 1;
    setPageParam(pageNum);
    if (pageNum === 1) {
      history.push('');
      return;
    }
    history.push(`/page/${pageNum}`);
  };

  useEffect(() => {
    if (params.page && Number.isNaN(Number(params.page))) {
      setPageParam(1);
      history.push('');
    }
    dispatch(userListFetchStart(pageParam, perPage));
  }, [dispatch, pageParam, perPage, history]);

  useEffect(() => history.listen((location) => {
    if (history.action) {
      if (location.pathname.includes('page')) {
        const page = Number(location.pathname.split('/').slice(-1));
        if (Number.isNaN(page)) {
          setPageParam(1);
          history.push('');
          return;
        }
        setPageParam(page);
      } else {
        setPageParam(1);
      }
    }
  }), [history]);

  if (loading) {
    return <Loader />;
  }

  if (error) {
    if (error.message === '404') {
      return <NotFoundPage />;
    }

    return <ErrorIndicator message={error.message} />;
  }

  return (
    <UserList
      list={userList}
      page={page}
      pages={pages}
      handleChange={handleChangePage}
    />
  );
};

export default HomePageContainer;
