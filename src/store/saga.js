import { all } from 'redux-saga/effects';

import { watchFetchUserList } from 'ducks/userList';
import { watchFetchUser } from 'ducks/user';

export default function* rootSaga() {
  yield all([
    watchFetchUserList(),
    watchFetchUser()
  ]);
}
