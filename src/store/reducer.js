import { combineReducers } from 'redux';

import userListReducer from 'ducks/userList';
import userReducer from 'ducks/user';

const rootReducer = combineReducers({
  userList: userListReducer,
  user: userReducer
});

export default rootReducer;
