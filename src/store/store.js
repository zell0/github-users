import { createStore, applyMiddleware } from 'redux';

import rootReducer from './reducer';
import rootSaga from './saga';
import { middlewares, sagaMiddleware } from './middlewares';

const store = createStore(
  rootReducer,
  applyMiddleware(...middlewares)
);

sagaMiddleware.run(rootSaga);

export default store;
