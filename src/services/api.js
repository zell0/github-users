class Api {
  _apiBase = 'https://api.github.com';

  getResource = async (url) => {
    const res = await fetch(`${this._apiBase}${url}`);

    if (!res.ok) {
      throw new Error(res.status);
    }

    return res.json();
  };

  getUserList = async (page, perPage) => (
    this.getResource(`/search/users?q=type:user&per_page=${perPage}&page=${page}&sort=followers`)
      .then((data) => data)
  );

  getUser = async (username) => (
    this.getResource(`/users/${username}`)
      .then((data) => data)
  );
}

export default new Api();
