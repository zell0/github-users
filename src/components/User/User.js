import PropTypes from 'prop-types';
import Moment from 'react-moment';

import {
  StyledUser, Avatar, Info, Name, Description, Date
} from './styles';

const User = ({ user }) => (
  <StyledUser>
    <Avatar>
      <img src={user.avatar_url} alt={user.name} />
    </Avatar>
    <Info>
      <Name>{user.name}</Name>
      {user.company && (
        <Description>
          {user.company.trim()}
          {', '}
          {user.location}
        </Description>
      )}
      <Date>
        From
        {' '}
        <Moment format="MM/DD/YYYY">{user.created_at}</Moment>
      </Date>
    </Info>
  </StyledUser>
);

User.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default User;
