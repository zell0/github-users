import styled from 'styled-components/macro';

export const StyledUser = styled.div`
  display: flex;
  align-items: center;
  padding: 20px 0;
`;

export const Avatar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 150px;
  height: 150px;
  border-radius: 50%;
  margin-right: ${({ theme }) => theme.common.gutter}px;
  overflow: hidden;
`;

export const Info = styled.div``;

export const Name = styled.h3`
  font-size: 1.5rem;
  margin: 0 0 ${({ theme }) => theme.common.indent}px;
`;

export const Description = styled.p`
  margin: 0 0 ${({ theme }) => theme.common.indent}px;
`;

export const Date = styled.div`
  font-size: 0.85rem;
  color: ${({ theme }) => theme.colors.secondary};
`;
