import spinner from 'assets/icons/loader.svg';

import { StyledLoader } from './styles';

const Loader = () => (
  <StyledLoader>
    <img src={spinner} alt="Loading..." />
  </StyledLoader>
);

export default Loader;
