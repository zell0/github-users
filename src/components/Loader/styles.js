import styled from 'styled-components/macro';

export const StyledLoader = styled.div`
  width: 100px;
  height: 100px;
  margin: auto;
`;
