import {
  Route, Switch, Redirect, useLocation
} from 'react-router-dom';

import ErrorBoundary from 'components/ErrorBoundary';
import HomePageContainer from 'pages/HomePageContainer';
import UserPageContainer from 'pages/UserPageContainer';
import NotFoundPage from 'pages/NotFoundPage';

import GlobalStyle from 'styles/global';
import { Container } from 'styles/common';

const App = () => {
  const { pathname } = useLocation();

  return (
    <Container>
      <GlobalStyle />
      <ErrorBoundary>
        <Switch>
          <Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />
          <Route path="/" exact component={HomePageContainer} />
          <Route path="/page/:page/" exact component={HomePageContainer} />
          <Route path="/:user/" exact component={UserPageContainer} />
          <Route path="*" component={NotFoundPage} />
        </Switch>
      </ErrorBoundary>
    </Container>
  );
};

export default App;
