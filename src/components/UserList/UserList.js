import PropTypes from 'prop-types';
import Pagination from 'components/Pagination';

import {
  List, Item, Info, Avatar, Button
} from './styles';

const UserList = ({
  list, page, pages, handleChange
}) => (
  <>
    <List>
      {
        list.map((user) => (
          <Item key={user.id}>
            <Info to={`/${user.login}`}>
              <Avatar>
                <img src={user.avatar_url} alt={user.login} />
              </Avatar>
              {user.login}
            </Info>
            <Button href={user.html_url} rel="noreferrer nofollow" target="_blank">Github page</Button>
          </Item>
        ))
      }
    </List>
    {pages > 1 && (
      <Pagination
        forcePage={page - 1}
        pageCount={pages}
        onPageChange={handleChange}
        hrefBuilder={(pageNum) => (pageNum !== 1 ? `/page/${pageNum}` : '/')}
        marginPagesDisplayed={1}
      />
    )}
  </>
);

UserList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.object),
  page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default UserList;
