import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';

import { device } from 'constants/device';

export const List = styled.div`
  margin-top: 20px;
  padding: 0 20px;
  border: 1px solid ${({ theme }) => theme.colors.border};
`;

export const Item = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: center;
  padding: ${({ theme }) => theme.common.indent}px 0;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};

  :last-child {
    border: none;
  }

  ${device.tablet} {
    flex-direction: column;
    align-items: flex-start;
  }
`;

export const Info = styled(NavLink)`
  display: flex;
  align-items: center;
  margin-top: 5px;
  margin-bottom: 5px;
  margin-right: ${({ theme }) => theme.common.indent}px;

  ${device.tablet} {
    margin-bottom: ${({ theme }) => theme.common.indent}px;
  }
`;

export const Avatar = styled.div`
  flex-shrink: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
  border-radius: 50%;
  margin-right: ${({ theme }) => theme.common.indent}px;
  overflow: hidden;
`;

export const Button = styled.a`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  padding: 5px 20px;
  border-radius: 20px;
  border: 1px solid ${({ theme }) => theme.colors.main};

  ${device.tablet} {
    padding: 5px 15px;
  }
`;
