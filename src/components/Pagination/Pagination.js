import React from 'react';
import ReactPaginate from 'react-paginate';

import prevIcon from 'assets/icons/prev.svg';
import nextIcon from 'assets/icons/next.svg';

import { StyledPagination } from './styles';

const Pagination = (props) => (
  <StyledPagination>
    <ReactPaginate
      {...props}
      containerClassName="pagination"
      pageClassName="pagination__item"
      pageLinkClassName="pagination__link"
      activeClassName="active"
      previousLabel={<img src={prevIcon} alt="prev" />}
      nextLabel={<img src={nextIcon} alt="next" />}
      previousClassName="pagination__item"
      nextClassName="pagination__item"
      previousLinkClassName="pagination__link"
      nextLinkClassName="pagination__link"
      breakClassName="pagination__break"
      pageLabelBuilder={(page) => (String(page).length > 3 ? `${String(page).slice(0, 3)}...` : page)}
    />
  </StyledPagination>
);

export default Pagination;
