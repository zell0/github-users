import styled from 'styled-components/macro';

import { device } from 'constants/device';

export const StyledPagination = styled.div`
  margin: ${({ theme }) => theme.common.gutter}px 0;
  
  .pagination {
    display: flex;
    flex-flow: row wrap;
    justify-content: center;
    align-items: center;
    
    &__item {
      margin: 3px;

      &.active {

        .pagination__link {
          color: ${({ theme }) => theme.colors.light};
          background-color: ${({ theme }) => theme.colors.borderDark};
          border-color: ${({ theme }) => theme.colors.borderDark};
        }
      }
    }
    
    &__link {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 50px;
      height: 50px;
      font-size: 0.9rem;
      text-align: center;
      border-radius: 50%;
      border: 1px solid ${({ theme }) => theme.colors.border};
      
      :hover {
        background-color: ${({ theme }) => theme.colors.border};
      }

      ${device.tablet} {
        width: 40px;
        height: 40px;
        font-size: 0.8rem;
      }
    }
    
    &__break {
      margin: 0 5px;
    }
    
    img {
      max-width: 30%;
    }
  }
`;
