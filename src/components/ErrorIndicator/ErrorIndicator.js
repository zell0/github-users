import PropTypes from 'prop-types';

const ErrorIndicator = ({ message }) => (
  <div style={{ textAlign: 'center' }}>
    <h2>
      Error
      {' '}
      {message}
    </h2>
    <h3>Please, try again later</h3>
  </div>
);

ErrorIndicator.propTypes = {
  message: PropTypes.string
};

export default ErrorIndicator;
