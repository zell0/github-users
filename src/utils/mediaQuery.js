const mediaQuery = (size) => `@media (max-width: ${size}px)`;

export default mediaQuery;
