import { useSelector } from 'react-redux';
import { put, call, takeLatest } from 'redux-saga/effects';

import api from 'services/api';

/**
 * Action types
 */
const USER_FETCH_START = 'USER_FETCH_START';
const USER_FETCH_STOP = 'USER_FETCH_STOP';
const USER_FETCH_SUCCESS = 'USER_FETCH_SUCCESS';
const USER_FETCH_FAILURE = 'USER_FETCH_FAILURE';

/**
 * Actions
 */
export const userFetchStart = (username) => ({
  type: USER_FETCH_START,
  username
});

export const userFetchStop = () => ({
  type: USER_FETCH_STOP
});

export const userFetchSuccess = (user) => ({
  type: USER_FETCH_SUCCESS,
  payload: user
});

export const userFetchFailure = (error) => ({
  type: USER_FETCH_FAILURE,
  payload: error
});

/**
 * Reducer
 */
const initialState = {
  user: null,
  loading: false,
  error: null
};

const userReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case USER_FETCH_START:
      return {
        ...state,
        user: null,
        loading: true,
        error: null
      };

    case USER_FETCH_STOP:
      return {
        ...state,
        loading: false,
        error: null
      };

    case USER_FETCH_SUCCESS:
      return {
        ...state,
        user: payload,
        loading: false,
        error: null
      };

    case USER_FETCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };

    default:
      return state;
  }
};

/**
 * Hooks
 */
export const useUser = () => {
  const { loading, error, user } = useSelector((state) => state.user);

  return { loading, error, user };
};

/**
 * Sagas
 */
function* fetchUser({ username }) {
  try {
    const user = yield call(api.getUser, username);
    yield put(userFetchSuccess(user));
    yield put(userFetchStop());
  } catch (e) {
    yield put(userFetchFailure(e));
  }
}

export function* watchFetchUser() {
  yield takeLatest(USER_FETCH_START, fetchUser);
}

export default userReducer;
