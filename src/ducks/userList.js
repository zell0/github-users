import { useSelector } from 'react-redux';
import { put, call, takeLatest } from 'redux-saga/effects';

import api from 'services/api';

/**
 * Action types
 */
const USERLIST_FETCH_START = 'USERLIST_FETCH_START';
const USERLIST_FETCH_STOP = 'USERLIST_FETCH_STOP';
const USERLIST_FETCH_SUCCESS = 'USERLIST_FETCH_SUCCESS';
const USERLIST_FETCH_FAILURE = 'USERLIST_FETCH_FAILURE';
const USERLIST_SET_PAGE = 'USERLIST_SET_PAGE';

/**
 * Actions
 */
export const userListFetchStart = (page, perPage) => ({
  type: USERLIST_FETCH_START,
  page,
  perPage
});

export const userListFetchStop = () => ({
  type: USERLIST_FETCH_STOP
});

export const userListFetchSuccess = (userList) => ({
  type: USERLIST_FETCH_SUCCESS,
  payload: userList
});

export const userListFetchFailure = (error) => ({
  type: USERLIST_FETCH_FAILURE,
  payload: error
});

export const userListSetPage = (page) => ({
  type: USERLIST_SET_PAGE,
  payload: page
});

/**
 * Reducer
 */
const initialState = {
  userList: [],
  loading: false,
  error: null,
  page: 1,
  perPage: 10,
  total: 0
};

const userListReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case USERLIST_FETCH_START:
      return {
        ...state,
        userList: [],
        loading: true,
        error: null
      };

    case USERLIST_FETCH_STOP:
      return {
        ...state,
        loading: false,
        error: null
      };

    case USERLIST_FETCH_SUCCESS:
      return {
        ...state,
        userList: payload.items,
        total: payload.total_count,
        loading: false,
        error: null
      };

    case USERLIST_FETCH_FAILURE:
      return {
        ...state,
        loading: false,
        error: payload
      };

    case USERLIST_SET_PAGE:
      return {
        ...state,
        page: payload
      };

    default:
      return state;
  }
};

/**
 * Hooks
 */
export const useUserList = () => {
  const {
    loading, error, userList, page, perPage, total
  } = useSelector((state) => state.userList);

  return {
    loading, error, userList, page, perPage, total
  };
};

/**
 * Sagas
 */
function* fetchUserList({ page, perPage }) {
  try {
    const userList = yield call(api.getUserList, page, perPage);
    yield put(userListFetchSuccess(userList));
    yield put(userListSetPage(page));
    yield put(userListFetchStop());
  } catch (e) {
    yield put(userListFetchFailure(e));
  }
}

export function* watchFetchUserList() {
  yield takeLatest(USERLIST_FETCH_START, fetchUserList);
}

export default userListReducer;
