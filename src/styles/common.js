import styled from 'styled-components/macro';

export const Container = styled.div`
  position: relative;
  width: 100%;
  max-width: ${({ theme }) => theme.common.containerWidth}px;
  margin: 0 auto;
  padding: 0 ${({ theme }) => theme.common.indent}px;
`;

export const Content = styled.div`
  margin-bottom: 30px;

  img {
    display: block;
    margin: 25px 0 20px;
    background-color: ${({ theme }) => theme.colors.border};
  }
  
  h2, h3, h4, h5, h6 {
    margin: 30px 0 10px;
  }
  
  p {
    margin: 0 0 20px;
  }
`;
