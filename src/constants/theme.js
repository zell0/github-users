const theme = {
  common: {
    containerWidth: 1200,
    gutter: 30,
    indent: 15
  },
  fonts: {
    main: '\'Arial\', sans-serif'
  },
  colors: {
    main: '#000',
    secondary: '#767676',
    border: '#ccc',
    borderDark: '#686B73',
    light: '#fff'
  }
};

export default theme;
